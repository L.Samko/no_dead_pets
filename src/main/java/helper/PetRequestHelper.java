package helper;

import configurations.CategoryData;
import configurations.PetModelData;
import configurations.TagsItemData;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.*;

public class PetRequestHelper {
    public static PetModelData petModelData = new PetModelData();

    private static long randomId() {
        Random rand = new Random();
        long id = rand.nextLong();
        if (id < 0){
            id *= -1;
        }
        return id;
    }

private static String randomName(){
        String name = RandomStringUtils.randomAlphabetic(10);
        return name;
}

    private static List<TagsItemData> createTagItemDataModel(long tagId, String tagName) {
        TagsItemData tagsItemData = new TagsItemData();
        tagsItemData.setId(tagId);
        tagsItemData.setName(tagName);

        return Collections.singletonList(tagsItemData);
    }

    private static List<String> createPhotoDataModel (){
        List<String> photoUrls = new ArrayList<>();
        String photoUrl = "https://assets3.thrillist.com/v1/image/2754967/size/tmg-article_tall;jpeg_quality=20.jpg";
        photoUrls.add(photoUrl);
        return photoUrls;
    }

    private static CategoryData createCategoryDataModel (String categoryName, long categoryId){
        CategoryData categoryData = new CategoryData();
        categoryData.setName(categoryName);
        categoryData.setId(categoryId);

        return categoryData;
    }

    public static PetModelData createPetModelWithAllRequiredFields() {
        String petName =randomName();
        petModelData.setName(petName);
        petModelData.setPhotoUrls(createPhotoDataModel());
        return petModelData;
    }

    public static PetModelData createPetModelWithAllFields() {
        long petId = randomId();
        String petName = randomName();

        long categoryId =randomId();
        String categoryName =randomName();

        long tagId = randomId();
        String tagName = randomName();

        petModelData.setId(petId);
        petModelData.setName(petName);
        petModelData.setStatus(PetModelData.PetStatus.SOLD.name());
        petModelData.setCategory(createCategoryDataModel(categoryName,categoryId));
        petModelData.setTags(createTagItemDataModel(tagId, tagName));
        petModelData.setPhotoUrls(createPhotoDataModel());

        return petModelData;
    }

    public static PetModelData createPetModelWithoutRequiredFields() {
        Random rand = new Random();
        long petId = rand.nextLong();
        petModelData.setId(petId);
        return petModelData;
    }

    public static PetModelData updatePetModelWithInvalidInput() {
        petModelData.setId(Long.parseLong(String.valueOf(0)));
        return petModelData;
    }

    public static PetModelData updatePetModel() {
        petModelData.setId(15151);
        petModelData.setStatus(PetModelData.PetStatus.AVAILABLE.name());
        return petModelData;
    }


}
