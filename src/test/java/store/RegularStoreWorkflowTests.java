//package store;
//
//import io.restassured.http.ContentType;
//import io.restassured.response.Response;
//import org.testng.annotations.Test;
//import specifications.RequestStoreSpecifications;
//import static configurations.ConfigurationsData.STORE_URL;
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.equalTo;
//
//
//public class RegularStoreWorkflowTests {
//    private static String storeId;
//
//    @Test(priority = 1)
//    public void placeAnOrderForPet() {
//        Response response =   given()
//                .when()
//                .body(RequestStoreSpecifications.ADD_NEW_STORE_SPEC)
//                .contentType(ContentType.JSON)
//                .post(STORE_URL + "order")
//                .then()
//                .assertThat().statusCode(200)
//                .and().body("status", equalTo("placed"))
//                .extract().response();
//
//        response.prettyPrint();
//        storeId = response.jsonPath().getString("id");
//        System.out.println(storeId);
//    }
//
//    @Test(priority = 2)
//    public void getStoreInventory() {
//        given()
//                .when()
//                .get(STORE_URL + "inventory")
//                .then()
//                .assertThat().statusCode(200)
//                .log().everything();
//    }
//
//    @Test(priority = 3)
//    public void getStoreOrderByOrderId() {
//        Response response =given()
//                .when()
//                .get(STORE_URL + "order/" + storeId)
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .and().body("complete", equalTo(Boolean.parseBoolean("true")))
//                .extract().response();
//        response.prettyPrint();
//        String actualStoreId = response.jsonPath().getString("id");
//        System.out.println(actualStoreId);
//        assertThat( actualStoreId, equalTo(storeId));
//    }
//
//    @Test(priority = 4)
//    public void deleteOrderById() {
//        given()
//                .when()
//                .contentType(ContentType.JSON)
//                .delete(STORE_URL +"order/"+ storeId)
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .log().all();
//    }
//}
