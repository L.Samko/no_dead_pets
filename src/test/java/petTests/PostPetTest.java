package petTests;

import configurations.CategoryData;
import configurations.PetModelData;
import controllers.PetController;
import org.testng.annotations.Test;
import java.util.Collections;
import static helper.PetRequestHelper.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertTrue;

public class PostPetTest {
    private PetController petController = new PetController();

    @Test
    public void addNewPetWithAllRequiredFields() {
        PetModelData petRequest = createPetModelWithAllRequiredFields();
        CategoryData categoryData = new CategoryData();
        petRequest.setCategory(categoryData);

        PetModelData petResponse = petController.addNewPet(petRequest);

        assertThat(petResponse.getName(), equalToIgnoringCase(petRequest.getName()));
        assertThat(petResponse.getPhotoUrls(), equalTo(petRequest.getPhotoUrls()));
    }

    @Test
    public void addNewPetWithAllFields() {
        PetModelData petRequest = createPetModelWithAllFields();
        PetModelData petResponse = petController.addNewPet(petRequest);

        assertThat(petResponse.getId(), equalTo(petRequest.getId()));
        assertThat(petResponse.getName(), equalToIgnoringCase(petRequest.getName()));
        assertThat(petResponse.getStatus(), equalToIgnoringCase(petRequest.getStatus()));
        assertThat(petResponse.getPhotoUrls(), equalTo(petRequest.getPhotoUrls()));
        assertThat(petResponse.getCategory().getId(), equalTo(petRequest.getCategory().getId()));
        assertThat(petResponse.getCategory().getName(), equalToIgnoringCase(petRequest.getCategory().getName()));
        assertThat(petResponse.getTags().get(0).getId(), equalTo(petRequest.getTags().get(0).getId()));
        assertThat(petResponse.getTags().get(0).getName(), equalTo(petRequest.getTags().get(0).getName()));
    }



//
//    @Test
//    public void addNewPetWithoutRequiredFields() {
//        PetModelData petRequest = createPetModelWithoutRequiredFields();
//        PetModelData petResponse = petController.addNewPet(petRequest);
//        assertThat(petResponse.getName(), isEmptyOrNullString());
//    }
//
//    @Test
//    public void addNewPetWithInvalidInput() {
//        PetModelData petRequest = createPetModelWithAllFields();
//        petController.addNewPet(petRequest);
//        updatePetModelWithInvalidInput();
//        PetModelData petResponse = petController.updatePet(petRequest);
//        assertTrue(petRequest.getId() == 0);
//        assertThat(petResponse.getId(), not(petRequest.getId()));
//    }
//

    @Test()
    public void updatePetById() {
        PetModelData petRequest = createPetModelWithAllFields();
        String oldName = petRequest.getName();
        String actualName = ("Holly");
        petRequest.setName(actualName);

        PetModelData petResponse = petController.addNewPet(petRequest);

        System.out.println("Old name " + oldName);
        System.out.println("New name " + actualName);

        assertThat(petResponse.getId(), equalTo(petRequest.getId()));
        assertThat(petResponse.getName(), equalTo(petRequest.getName()));

    }

    @Test()
    public void uploadPetsImage() {
        PetModelData petRequest = createPetModelWithAllRequiredFields();
        petRequest.setPhotoUrls(Collections.
                singletonList("https://assets3.thrillist.com/v1/image/2754967/size/tmg-article_tall;jpeg_quality=20.jpg"));
        PetModelData petResponse = petController.addNewPet(petRequest);

        assertThat(petRequest.getPhotoUrls(), notNullValue());
        assertThat(petResponse.getPhotoUrls(), equalTo(petRequest.getPhotoUrls()));

    }

}
