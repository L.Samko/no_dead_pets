//package user;
//
//import configurations.ConfigurationsData;
//import io.restassured.http.ContentType;
//import org.testng.annotations.Test;
//
//import static io.restassured.RestAssured.given;
//
//public class PostUserTest extends ConfigurationsData {
//    @Test
//    public void createUser(){
//        given()
//                .when()
//                .body("[\n" +
//                        "  {\n" +
//                        "    \"id\": 3,\n" +
//                        "    \"username\": \"Victor\",\n" +
//                        "    \"firstName\": \"V\",\n" +
//                        "    \"lastName\": \"D\",\n" +
//                        "    \"email\": \"string\",\n" +
//                        "    \"password\": \"string\",\n" +
//                        "    \"phone\": \"string\",\n" +
//                        "    \"userStatus\": 2\n" +
//                        "  }\n" +
//                        "]")
//                .contentType(ContentType.JSON)
//                .post(USERS_URL)
//                .then()
//                .statusCode(200).extract().response().prettyPrint();
//    }
//
//    @Test
//    public void createUserWithArray(){
//        given()
//                .when()
//                .body("[\n" +
//                        "  {\n" +
//                        "    \"id\": 0,\n" +
//                        "    \"username\": \"string\",\n" +
//                        "    \"firstName\": \"string\",\n" +
//                        "    \"lastName\": \"string\",\n" +
//                        "    \"email\": \"string\",\n" +
//                        "    \"password\": \"string\",\n" +
//                        "    \"phone\": \"string\",\n" +
//                        "    \"userStatus\": 0\n" +
//                        "  }\n" +
//                        "]")
//                .contentType(ContentType.JSON)
//                .post(USERS_URL + "createWithArray")
//                .then()
//                .statusCode(500).extract().response().prettyPrint();
//    }
//
//    @Test
//    public void createUserWithList(){
//        given()
//                .when()
//                .body("[\n" +
//                        "  {\n" +
//                        "    \"id\": 3,\n" +
//                        "    \"username\": \"Victor\",\n" +
//                        "    \"firstName\": \"V\",\n" +
//                        "    \"lastName\": \"D\",\n" +
//                        "    \"email\": \"string\",\n" +
//                        "    \"password\": \"string\",\n" +
//                        "    \"phone\": \"string\",\n" +
//                        "    \"userStatus\": 2\n" +
//                        "  }\n" +
//                        "]")
//                .contentType(ContentType.JSON)
//                .post(USERS_URL + "createWithList")
//                .then()
//                .statusCode(500).extract().response().prettyPrint();
//    }
//}
