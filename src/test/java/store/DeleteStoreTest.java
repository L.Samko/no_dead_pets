//package store;
//
//import configurations.ConfigurationsData;
//import io.restassured.http.ContentType;
//import org.testng.annotations.Test;
//import static io.restassured.RestAssured.given;
//
//public class DeleteStoreTest extends ConfigurationsData {
//    @Test
//    public void deleteOrderById() {
//        String id = "9";
//        given()
//                .when()
//                .contentType(ContentType.JSON)
//                .delete(STORE_URL +"order/"+ id)
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .log().all();
//    }
//
//    @Test
//    public void deleteOrderByWrongId() {
//        String id = "12218";
//        given()
//                .when()
//                .contentType(ContentType.JSON)
//                .delete(STORE_URL +"order/"+ id)
//                .then()
//                .assertThat().statusCode(404)
//                .and().contentType(ContentType.JSON)
//                .log().all();
//    }
//
//}