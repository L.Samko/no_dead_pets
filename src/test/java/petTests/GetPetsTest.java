package petTests;

import configurations.PetModelData;
import controllers.PetController;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static helper.PetRequestHelper.createPetModelWithAllFields;
import static helper.PetRequestHelper.updatePetModelWithInvalidInput;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GetPetsTest {

    private PetController petController = new PetController();

    @Test
    public void getPetByStatus() {
        PetModelData petRequest = createPetModelWithAllFields();
        petRequest.setStatus(PetModelData.PetStatus.PENDING.name());
        petController.addNewPet(petRequest);

        PetModelData petModelData = (PetModelData) petController.getPet(petRequest);

        List<PetModelData> petList = Arrays.stream(new PetModelData[]{petModelData})
                .filter(name -> name.getName() == null || name.getName() == petRequest.getName())
                .sorted()
                .collect(Collectors.toList());

        System.out.println(petList.size());
        System.out.println(petList);

        for (PetModelData pet : petList) {
            assertThat(petRequest.getId(), equalTo(pet.getId()));
            assertThat(petRequest.getName(), equalTo(pet.getName()));
            assertThat(petRequest.getStatus(), equalTo(pet.getStatus()));
            Assert.assertEquals(PetModelData.PetStatus.PENDING.name(), pet.getStatus());
        }
    }

//    @Test
//    public void getPetByWrongStatus() {
//        PetModelData petRequest = updatePetModelWithInvalidInput();
//        petController.addNewPet(petRequest);
//        PetModelData petModelData = (PetModelData) petController.getPet(petRequest);
//        List<PetModelData> petList = Arrays.stream(new PetModelData[]{petModelData})
//                .filter(status -> status.getStatus() == null || status.getStatus() == petRequest.getName())
//                .sorted()
//                .collect(Collectors.toList());
//        for (PetModelData pet : petList) {
//            assertThat(petRequest.getId(), equalTo(pet.getId()));
//            assertThat(petRequest.getName(), equalTo(pet.getName()));
//            assertThat(petRequest.getStatus(), equalTo(pet.getStatus()));
//        }
//    }

    @Test
    public void getPetByID() {
        PetModelData petRequest = createPetModelWithAllFields();
        petController.addNewPet(petRequest);
        PetModelData petModelData = (PetModelData) petController.getPet(petRequest);
        List<PetModelData> petList = Arrays.stream(new PetModelData[]{petModelData})
                .filter(id -> id.getId() == 0 || id.getId() == petRequest.getId())
                .sorted()
                .collect(Collectors.toList());
        for (PetModelData pet : petList) {
            assertThat(petRequest.getId(), equalTo(pet.getId()));
            assertThat(petRequest.getName(), equalTo(pet.getName()));
            assertThat(petRequest.getStatus(), equalTo(pet.getStatus()));
        }
    }

//    @Test
//    public void getPetByWrongID() {
//        String id = "hjklkkm";
//        PetModelData petRequest = createPetModelWithAllFields();
//        petController.addNewPet(petRequest);
//        petController.getPet(petRequest);
//        petRequest.setId(152121);
//        System.out.println("ID" + id);
//        Assert.assertNotEquals("Wrong ID" + id, petRequest.getId());
//    }
}
