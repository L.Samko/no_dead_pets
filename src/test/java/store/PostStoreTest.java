//package store;
//import configurations.ConfigurationsData;
//import io.restassured.http.ContentType;
//import org.testng.annotations.Test;
//import specifications.RequestStoreSpecifications;
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.equalTo;
//
//public class PostStoreTest extends ConfigurationsData {
//    @Test
//    public void placeAnOrderForPet() {
//        given()
//                .when()
//                .body(RequestStoreSpecifications.ADD_NEW_STORE_SPEC)
//                .contentType(ContentType.JSON)
//                .post(STORE_URL + "order")
//                .then()
//                .assertThat().statusCode(200)
//                .and().body("status", equalTo("placed"))
//                .log().all();
//    }
//}
