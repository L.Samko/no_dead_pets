package controllers;

import configurations.ConfigurationsData;
import configurations.PetModelData;
import configurations.PetNotFoundData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static configurations.ConfigurationsData.BY_ID;
import static configurations.ConfigurationsData.PET;
import static io.restassured.RestAssured.given;

public class PetController {
    private RequestSpecification requestSpecification;
    private ResponseSpecification responseSpecification;

    public PetController() {
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri(ConfigurationsData.MAIN_URL + PET)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL).build();
        responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
    }

    public PetModelData addNewPet(PetModelData petRequest) {
        return given(requestSpecification)
                .body(petRequest)
                .post().as(PetModelData.class);
    }

    public Response deletePet(PetModelData petModelData) {
        return given(requestSpecification)
                //.baseUri(ConfigurationsData.MAIN_URL + PET + BY_ID)
                .delete(String.valueOf(petModelData.getId()))
                .then()
                .statusCode(200)
                .extract().response();

//        return RestAssured.given()
//                .when()
//                .delete(ConfigurationsData.MAIN_URL+PET +"/" + petModelData.getId())
//                .then()
//                .statusCode(200)
//                .assertThat()
//                .extract().response();
    }

    public PetModelData updatePet(PetModelData petRequest) {
        return given(requestSpecification)
                .body(petRequest)
                .put().as(PetModelData.class);
    }

    public Object getPet(PetModelData petModelData) {
        Response response = given(requestSpecification).get(String.valueOf(petModelData.getId()));
        if (response.statusCode() == 200) {
            return response.as(PetModelData.class);
        } else {
            return response.as(PetNotFoundData.class);
        }
    }

    public PetNotFoundData getDeletedPet(PetModelData petModelData) {
        return given(requestSpecification)
                .get(String.valueOf(petModelData.getId()))
                .then()
                .statusCode(404)
                .and()
                .extract().response().as(PetNotFoundData.class);

    }

}
