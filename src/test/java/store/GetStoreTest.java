//package store;
//import io.restassured.http.ContentType;
//import org.testng.annotations.Test;
//import static configurations.ConfigurationsData.STORE_URL;
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.*;
//
//public class GetStoreTest {
//
//    @Test
//    public void getStoreInventory() {
//        given()
//                .when()
//                .get(STORE_URL + "inventory")
//                .then()
//                .assertThat().statusCode(200)
//                .log().everything();
//    }
//
//    @Test
//    public void verifyThatStoreListIsNotEmpty() {
//        given()
//                .when()
//                .get(STORE_URL + "inventory")
//                .then()
//                .assertThat().body("unavailable", notNullValue())
//                .and().statusCode(200)
//                .log().body();
//    }
//
//
//    @Test
//    public void getStoreOrderByOrderId() {
//        given()
//                .when()
//                .get(STORE_URL + "order/7")
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .and().body("status", equalToIgnoringCase("placed"))
//                .log().body();
//    }
//
//    @Test
//    public void getStoreOrderByWrongOrderId(){
//        given()
//                .when()
//                .get(STORE_URL + "order/abc")
//                .then()
//                .assertThat().statusCode(404)
//                .and().contentType(ContentType.JSON)
//                .and().body("type", equalTo("unknown"))
//                .log().body();
//    }
//
//    @Test
//    public void getStoreOrderByNonexistentOrderId (){
//        given()
//                .when()
//                .get(STORE_URL + "order/13556")
//                .then()
//                .assertThat().statusCode(404)
//                .and().contentType(ContentType.JSON)
//                .and().body("type", equalTo("error"))
//                .log().body();
//    }
//}
