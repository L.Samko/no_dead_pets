package specifications;

public class RequestPetSpecifications {

    public static final String ADD_NEW_PET_SPEC = "{\n" +
            "  \"id\": 0,\n" +
            "  \"category\": {\n" +
            "    \"id\": 0,\n" +
            "    \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": \"Dog\",\n" +
            "  \"photoUrls\": [\n" +
            "    \"https://merdacz.pl/wp-content/uploads/2018/07/what-your-dog-is-really-trying-say-when-making-a-sad-face-1.jpg\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"name\": \"string\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";

//    public static final String UPDATE_PET_SPEC = "{\n" +
//            "  \"id\": 1,\n" +
//            "  \"category\": {\n" +
//            "    \"id\": 1,\n" +
//            "    \"name\": \"DogCategory\"\n" +
//            "  },\n" +
//            "  \"name\": \"%s\",\n" +
//            "  \"photoUrls\": [\n" +
//            "    \"https://merdacz.pl/wp-content/uploads/2018/07/what-your-dog-is-really-trying-say-when-making-a-sad-face-1.jpg\"\n" +
//            "  ],\n" +
//            "  \"tags\": [\n" +
//            "    {\n" +
//            "      \"id\": 1,\n" +
//            "      \"name\": \"DogTag\"\n" +
//            "    }\n" +
//            "  ],\n" +
//            "  \"status\": \"%s\"\n" +
//            "}";

    public static final String ADD_NEW_PET_WITH_WRONG_ID_SPEC = "{\n" +
            "  \"id\": AbbC,\n" +
            "  \"category\": {\n" +
            "    \"id\": 0,\n" +
            "    \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": \"Dog\",\n" +
            "  \"photoUrls\": [\n" +
            "    \"https://merdacz.pl/wp-content/uploads/2018/07/what-your-dog-is-really-trying-say-when-making-a-sad-face-1.jpg\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"name\": \"string\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";

    public static final String ADD_NEW_PET_WITH_WRONG_NAME_SPEC = "{\n" +
            "  \"id\": 5,\n" +
            "  \"category\": {\n" +
            "    \"id\": 0,\n" +
            "    \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": @#tad,\n" +
            "  \"photoUrls\": [\n" +
            "    \"https://merdacz.pl/wp-content/uploads/2018/07/what-your-dog-is-really-trying-say-when-making-a-sad-face-1.jpg\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"name\": \"string\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";

    public static final String GET_PET_WITH_WRONG_STATUS_SPEC = "{\n" +
            "  \"id\": 2,\n" +
            "  \"category\": {\n" +
            "    \"id\": 0,\n" +
            "    \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": \"Fox\",\n" +
            "  \"photoUrls\": [\n" +
            "    \"https://merdacz.pl/wp-content/uploads/2018/07/what-your-dog-is-really-trying-say-when-making-a-sad-face-1.jpg\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"name\": \"string\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"status\": 1234\n" +
            "}";

}
