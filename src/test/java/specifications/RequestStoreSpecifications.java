package specifications;

public class RequestStoreSpecifications {
    public static final String ADD_NEW_STORE_SPEC = "{\n" +
            "  \"id\": 12,\n" +
            "  \"petId\": 5,\n" +
            "  \"quantity\": 13,\n" +
            "  \"shipDate\": \"2018-10-22T15:27:14.636Z\",\n" +
            "  \"status\": \"placed\",\n" +
            "  \"complete\": true\n" +
            "}";
}
