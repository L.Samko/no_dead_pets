package petTests;

import configurations.PetModelData;
import org.testng.annotations.Test;
import controllers.PetController;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static helper.PetRequestHelper.createPetModelWithAllFields;
import static helper.PetRequestHelper.updatePetModelWithInvalidInput;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DeletePetTest {

    private PetController petController = new PetController();

    @Test
    public void deletePetById() {
        PetModelData petRequest = createPetModelWithAllFields();
        petController.addNewPet(petRequest);
        petController.deletePet(petRequest);
        PetModelData petModelData = new PetModelData();
        List<PetModelData> petList = Arrays.stream(new PetModelData[]{petModelData})
                .filter(id -> id.getId() == 0)
                .sorted()
                .collect(Collectors.toList());
        System.out.println(petList.size());
        System.out.println(petList);

     //  List<PetModelData> list =  petController.getPet(petRequest);

        for (PetModelData pet : petList) {
            assertThat(pet.getId(), equalTo(0L));
        }
    }

//    @Test
//    public void deletePetByWrongId() {
//        PetModelData petRequest = createPetModelWithAllFields();
//        petController.addNewPet(petRequest);
//        PetModelData petModelData = updatePetModelWithInvalidInput();
//        petController.deletePet(petModelData);
//        assertThat(petModelData.getName(), isEmptyString());
//    }
}
