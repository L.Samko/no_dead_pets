package configurations;

public class ConfigurationsData {
    public static final String MAIN_URL = "https://petstore.swagger.io/v2";
    public static final String PET = "/pet";
    public static final String FIND_BY_STATUS = "/findByStatus";
    public static final String BY_ID = "/{petId}";
    public static final String UPLOAD_IMAGE = "/uploadImage";
    public static final String INVALID_STATUS = "/651515";


}
