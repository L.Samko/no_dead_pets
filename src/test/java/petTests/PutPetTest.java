package petTests;

import configurations.PetModelData;
import controllers.PetController;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import specifications.RequestPetSpecifications;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static configurations.ConfigurationsData.MAIN_URL;
import static helper.PetRequestHelper.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PutPetTest {
    private PetController petController = new PetController();

    @Test
    public void updateExistingPet() {
        PetModelData petRequest = createPetModelWithAllFields();
        petController.addNewPet(petRequest);
        PetModelData petModelData = updatePetModel();
        petController.updatePet(petRequest);

        List<PetModelData> petList = Arrays.stream(new PetModelData[]{petModelData})
                .filter(status -> status.getStatus() == null || status.getStatus() == petRequest.getStatus())
                .sorted()
                .collect(Collectors.toList());

        for (PetModelData pet : petList) {
            assertThat(petRequest.getStatus(), equalTo(pet.getStatus()));
            assertThat(petRequest.getId(), equalTo(pet.getId()));
        }
    }

//    @Test
//    public void updateExistingPetWithNegativeRangeOfID() {
//        PetModelData petRequest = createPetModelWithAllFields();
//        petController.addNewPet(petRequest);
//        assertFalse(petRequest.getId() > 0, "ID is out of range: " + petRequest.getId());
//    }
//
//    @Test
//    public void updateExistingPetWithInvalidInputOfID() {
//        PetModelData petRequest = createPetModelWithAllFields();
//        petController.addNewPet(petRequest);
//        PetModelData petModelData = updatePetModelWithInvalidInput();
//        petController.updatePet(petRequest);
//
//       assertThat(petRequest.getId(),equalTo(petModelData.getId()));
//    }

//    @Test
//    public void updateExistingPetWithWrongName() {
//        given()
//                .when()
//                .body(RequestPetSpecifications.ADD_NEW_PET_WITH_WRONG_NAME_SPEC)
//                .contentType(ContentType.JSON)
//                .put(MAIN_URL)
//                .then()
//                .assertThat().statusCode(400)
//                .and().contentType(ContentType.JSON)
//                .body("message", equalTo("bad input"))
//                .log().all();
//    }
//
//    @Test
//    public void updateExistingPetWithWrongStatus() {
//        given()
//                .when()
//                .body(RequestPetSpecifications.GET_PET_WITH_WRONG_STATUS_SPEC)
//                .contentType(ContentType.JSON)
//                .put(MAIN_URL)
//                .then()
//
//                .and().contentType(ContentType.JSON)
//                .body("message", equalTo("bad input"))
//                .log().all();
//    }
}
