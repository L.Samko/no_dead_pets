//package petTests;
//
//import io.restassured.http.ContentType;
//import io.restassured.response.Response;
//import org.testng.annotations.Test;
//import specifications.RequestPetSpecifications;
//import java.io.File;
//import static configurations.ConfigurationsData.MAIN_URL;
//import static configurations.ConfigurationsData.PET;
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.equalTo;
//
//public class RegularPetWorkflowTests {
//
//    @Test(priority = 1)
//    public void addNewPet() {
//        Response response = given()
//                .when()
//                .body(RequestPetSpecifications.ADD_NEW_PET_SPEC)
//                .contentType(ContentType.JSON)
//                .post(MAIN_URL+PET)
//                .then()
//                .assertThat().statusCode(200)
//                .and().body("status", equalTo("available")).extract().response();
//
//        response.prettyPrint();
////        petId = response.jsonPath().getString("id");
////        System.out.println(petId);
//    }
//
//    @Test(priority = 2)
//    public void updateExistingPet() {
//        Response response = given()
//                .when()
//                .body(RequestPetSpecifications.ADD_NEW_PET_SPEC)
//                .contentType(ContentType.JSON)
//                .put(MAIN_URL+PET)
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .extract().response();
//
//        response.prettyPrint();
//        String actualPetId = response.jsonPath().getString("id");
//        System.out.println(actualPetId);
//       // assertThat(actualPetId, not(petId));
//    }
//
//    @Test(priority = 3)
//    public void getPetByID() {
//        Response response =
//                given()
//                        .when()
//                        .get(MAIN_URL+PET + petId)
//                        .then()
//                        .assertThat().statusCode(200)
//                        .and().contentType(ContentType.JSON)
//                        .extract().response();
//        response.prettyPrint();
//        String actualPetId = response.jsonPath().getString("id");
//        System.out.println(actualPetId);
//        assertThat(actualPetId, equalTo(petId));
//    }
//
//    @Test(priority = 4)
//    public void updatePetById() {
//        String name = "NewDog";
//        String status = "sold";
//        Response response =
//                given()
//                        .param("name", name)
//                        .param("status", status)
//                        .when()
//                        .contentType(ContentType.JSON)
//                        .post(MAIN_URL+PET + petId)
//
//                        .then()
//                        .assertThat().statusCode(200)
//                        .and().extract().response();
//
//        response.prettyPrint();
//
//        String actualPetId = response.jsonPath().getString("id");
//
//        assertThat("Pet Id was not updated", actualPetId, not(petId));
//
//    }
//
//    @Test(priority = 5)
//    public void uploadPetsImage() {
//        given()
//                .accept("application/json")
//                .multiPart(new File("C:/Users/l.samko/10945690.jpg"))
//                //.post("/objects/files")
//                .when()
//                //.contentType(ContentType.JSON)
//                .post(MAIN_URL+PET + petId + "/uploadImage")
//                .then()
//                .assertThat()
//                .statusCode(200)
//                .log().all();
//    }
//
//    @Test(priority = 6)
//    public void deletePetById() {
//        given()
//                .when()
//                .contentType(ContentType.JSON)
//                .delete(MAIN_URL+PET + petId)
//                .then()
//                .assertThat().statusCode(200)
//                .and().contentType(ContentType.JSON)
//                .log().all();
//
//    }
//
//}
