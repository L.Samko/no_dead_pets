package configurations;

import java.util.List;

public class PetModelData {
    private List<String> photoUrls;
    private String name;
    private long id;
    private CategoryData category;
    private List<TagsItemData> tags;
    private String status;

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setCategory(CategoryData category) {
        this.category = category;
    }

    public CategoryData getCategory() {
        return category;
    }

    public void setTags(List<TagsItemData> tags) {
        this.tags = tags;
    }

    public List<TagsItemData> getTags() {
        return tags;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "PetModelData{" +
                        "photoUrls = '" + photoUrls + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",category = '" + category + '\'' +
                        ",tags = '" + tags + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    public  enum PetStatus{
        AVAILABLE, PENDING, SOLD
    }
}

